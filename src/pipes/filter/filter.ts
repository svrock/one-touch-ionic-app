import { Pipe, PipeTransform } from '@angular/core';
/**
 * Generated class for the FilterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value, ...args) {
    if (args[0] === 1) {
      value = value.sort((a, b) => b.price - a.price);
    }
    if (args[0] === 2) {
      value = value.sort((a, b) => a.price - b.price);
    }
    if (args[0] === 3) {
      value = value.sort((a, b): any => {
        const date1 = new Date(a.createdAt);
        const date2 = new Date(b.createdAt);
        return date2.getTime() - date1.getTime();
      });
    }
    if (args[0] === 4) {
      value = value.sort((a, b): any => {
        const date1 = new Date(a.createdAt);
        const date2 = new Date(b.createdAt);
        return date1.getTime() - date2.getTime();
      });
    }
    return value;
  }
}
