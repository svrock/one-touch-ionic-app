import { Pipe, PipeTransform } from '@angular/core';
/**
 * Generated class for the SearchfilterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'searchfilter',
})
export class SearchfilterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(items, searchText): any[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    return items.filter(it => {
      return it.key.toLowerCase().includes(searchText.toLowerCase());
    });
  }
}
