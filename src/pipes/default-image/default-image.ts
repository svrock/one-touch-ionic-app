import { Pipe, PipeTransform } from '@angular/core';
/**
 * Generated class for the DefaultImagePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'defaultImage',
})
export class DefaultImagePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    if (!value) {
      return 'assets/placeholder/food.png';
    } else {
      return value;
    }
  }
}
