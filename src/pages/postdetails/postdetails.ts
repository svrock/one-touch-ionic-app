import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { config } from '../../config';
import { UtilityProvider } from '../../providers/utility/utility';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
/**
* Generated class for the PostdetailsPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
@IonicPage({
  segment: 'page-postdetails/:id'
})
@Component({
  selector: 'page-postdetails',
  templateUrl: 'postdetails.html',
})
export class PostdetailsPage {
  @ViewChild(Slides) slides: Slides;
  post: any;
  postdetails: any = {};
  category: any = {};
  userdetails: any = {};
  imagedetails: any = [];
  productCategoryList: any = [];
  ratingshow: number;
  constructor(private iab: InAppBrowser, public utility: UtilityProvider,
    public navCtrl: NavController, public navParams: NavParams,
    public _services: ServicesProvider, private launchNavigator: LaunchNavigator) {
    this.post = this.navParams.data.id;
  }
  ngOnInit() {
    this._services.getPostDetails(this.post).subscribe((response: any) => {
      this.postdetails = response.data.post;
      this.userdetails = response.data.user;
      this.category = response.data.category;
      const image = response.data.image;
      if (image.length < 1) {
        const res = {};
        res['image'] = 'assets/imgs/placeholder.png';
        this.imagedetails.push(res);
        return false;
      }
      const imageval = [];
      image.forEach(element => {
        const res = {};
        res['image'] = `${config.SOCKET_ENDPOINT}image/postimage/${element.image}`;
        imageval.push(res);
      });
      this.imagedetails = imageval;
    });
    this.getProductCategory();
  }
  goTovendorDetails(item) {
    this.navCtrl.push('ProfilePage', { data: item });
  }
  openChat(item) {
    this.navCtrl.push('ChatPage', { id: '5b1c0d796728f530bbd321dd' });
  }
  goToPage(page) {
    this.navCtrl.push(page, { id: this.post, type: 'post' });
  }
  goToMap(item) {
    const mylocation = JSON.parse(localStorage.getItem('userdetails'));
    const options: LaunchNavigatorOptions = {
      start: mylocation.address,
    };
    this.launchNavigator.navigate(item.address, options)
      .then(
      );
  }
  getProductCategory() {
    const obj = {
      shopid: this.post
    };
    this._services.getProductCategory(obj).subscribe((response: any) => {
      this.productCategoryList = response.response.data;
    }, (error) => {
    });
  }
  showProduct(item) {
    this.navCtrl.push('ProductPage', { id: item.id });
  }
  openWeb(url) {
    this.iab.create(url);
  }
  getWhatsAppMessage(item) {
    return `https://wa.me/${config.agentNumber}?text=${config.POST_CUSTOM_MESSAGE} ${item.title} ${item.address}`;
  }
}
