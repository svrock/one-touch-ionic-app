import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostdetailsPage } from './postdetails';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { RatingModule } from 'ngx-rating';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    PostdetailsPage,
  ],
  imports: [RatingModule, SharedModule,
    IonicPageModule.forChild(PostdetailsPage),
  ],
  providers: [LaunchNavigator, InAppBrowser]
})
export class PostdetailsPageModule { }
