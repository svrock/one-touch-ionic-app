import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController, Platform, AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
import { InAppBrowser } from '@ionic-native/in-app-browser';
declare var RazorpayCheckout: any;
import { Clipboard } from '@ionic-native/clipboard';
@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  subTotal: number;
  checkoutList: any[];
  address: any = [];
  paymenttype: string;
  shop: any = {};
  cartEmpty = true;
  deliveryfee = 10;
  totalPrice: number;
  pickup: string;
  instruction: string;
  offerDiscount: any;
  discountedValue: any;
  dicountedPrice: any;
  totalDiscount = 0;
  public isDiscountedOffer: boolean;
  constructor(private clipboard: Clipboard,
    public viewCtrl: ViewController, public platform: Platform, public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public utility: UtilityProvider, public navCtrl: NavController, private iab: InAppBrowser,
    public navParams: NavParams, public _services: ServicesProvider) {
  }
  showStep() {
    const text = `Open Gpay app and  pay
    ${this.price(this.totalPrice)} amount of order at 9083797873. Send the screenshot of the placed order in whatsapp`;
    const alert = this.alertCtrl.create({
      title: 'GPAY',
      subTitle: text,
      buttons: [{
        text: 'Copy number',
        handler: data => {
          this.clipboard.copy('9083797873');
          const msg = { msg: 'Copied', duration: 3000, position: 'centre' };
          this.utility.showToast(msg);
        }
      },
      ]
    });
    alert.present();
  }
  ionViewDidLoad() {
    const obj = {
      id: this.utility.getLoggedUser('_id')
    };
    this._services.getCartItem(obj).subscribe((response: any) => {
      let result = response.response.data.cart;
      result = result.filter((item) => item.shopid);
      const checkoutItems = [];
      this.subTotal = 0;
      result.forEach(element => {
        const newpro = {};
        newpro['price'] = element.price;
        newpro['product_name'] = element.productid.title;
        newpro['quantity'] = element.quantity;
        newpro['shopid'] = element.shopid.title;
        newpro['id'] = element.id;
        this.shop.name = element.shopid.title;
        this.shop.id = element.shopid.id;
        newpro['discount'] = element.productid.discount;
        if (element.productid.discount === 0) {
          newpro['isDiscountedOffer'] = false;
          newpro['discountReceived']  = 0;
          this.subTotal += element.quantity * element.price;
        } else {
          newpro['isDiscountedOffer'] = true;
          this.offerDiscount = element.productid.discount;
          this.dicountedPrice = element.price - this.offerDiscount;
          newpro['discountedprice'] = this.dicountedPrice;
          this.subTotal += element.quantity * this.dicountedPrice;
          newpro['discountReceived'] = element.productid.discount * element.quantity;
        }
        checkoutItems.push(newpro);
      });
      this.deliveryfee = this.getShippingCharges();
      for (const discount of checkoutItems) {
        this.totalDiscount += discount.discountReceived;
      }
      this.totalPrice = Number(this.subTotal) + Number(this.deliveryfee);
      checkoutItems.sort((a, b) => a.product_name.localeCompare(b.product_name));
      this.checkoutList = checkoutItems;
      this.cartEmpty = (result && result.length === 0) ? true : false;
    }, (error) => {
      this.cartEmpty = true;
    });
    const data = {
      userid: this.utility.getLoggedUser('_id')
    };
    this._services.getShippingAddress(data).subscribe((response: any) => {
      this.address = response.response.data;
    });
  }
  getShippingCharge() {
    const charge = JSON.parse(localStorage.getItem('shippingArea'));
    return charge.value;
  }
  changeAddres() {
    this.navCtrl.push('AddressPage');
  }
  payment() {
    const shippingArea = JSON.parse(localStorage.getItem('shippingArea'));
    if (shippingArea.status === 'closed') {
      const msg = {
        title: 'Booking closed',
        subtitle: shippingArea.msg
      };
      this.utility.showAlert(msg);
      return false;
    }
    if (this.checkoutList.length === 0) {
      const msg = {
        msg: 'Cart is empty',
        duration: 3000
      };
      this.utility.showToast(msg);
      return false;
    }
    if (this.paymenttype === undefined) {
      const msg = {
        msg: 'Please select payment type',
        duration: 3000
      };
      this.utility.showToast(msg);
      return false;
    }
    if (this.address.length < 1) {
      const msg = {
        msg: 'Please enter shipping address',
        duration: 3000
      };
      this.utility.showToast(msg);
      this.navCtrl.push('AddressPage');
      return false;
    }
    if (this.paymenttype === 'razor') {
      this.razorPay();
      return false;
    }
    this.processedPayment();
  }
  processedPayment() {
    if (this.paymenttype === 'cashpayment') {
      this.cashpayment('cashpayment', '');
      return false;
    }
    if (this.paymenttype === 'GPAYPAYTM') {
      this.cashpayment('cashpayment', '');
      return false;
    }
  }
  instamojo() {
    // let obj={
    //   purpose:'Nandan',
    //   amount:this.subTotal,
    //   buyer_name:`${Config.USER.firstname} ${Config.USER.lastname}`,
    //   phone:Config.USER.phone,
    //   buyer_email:'putumca@gmail.com'
    // }
    // this._services.makePaymenrtRequest(obj).subscribe((Response)=>{
    //   let result=Response.response.data
    //   this.payNow(result.payment_request.longurl)
    // })
  }
  payNow(longurl) {
    function onOpenHandler() {
      alert('Payments Modal is Opened');
    }
    function onCloseHandler() {
      alert('Payments Modal is Closed');
    }
    function onPaymentSuccessHandler(response) {
      alert('Payment Success');
    }
    function onPaymentFailureHandler(response) {
      alert('Payment Failure');
    }
    /* End client-defined Callback Handler Functions */
    /* Configuring Handlers */
    // Instamojo.configure({
    //   handlers: {
    //     onOpen: onOpenHandler,
    //     onClose: onCloseHandler,
    //     onSuccess: onPaymentSuccessHandler,
    //     onFailure: onPaymentFailureHandler
    //   }
    // });
    // Instamojo.open(longurl);
  }
  shopnow() {
    this.navCtrl.popToRoot();
    this.navCtrl.parent.select(2);
  }
  editOrder() {
    this.navCtrl.popToRoot();
    this.navCtrl.parent.select(3);
  }
  razorPay() {
    const options = {
      description: 'Food',
      image: 'http://139.59.12.86:3001/icon.png',
      currency: 'INR',
      key: 'rzp_test_9ltHMLxYrLMacM',
      count: 2,
      skip: 1,
      amount: this.subTotal * 100,
      name: this.shop.name,
      prefill: {
        email: this.utility.getLoggedUser('email'),
        contact: this.utility.getLoggedUser('phone'),
        name: `${this.utility.getLoggedUser('firstname')} ${this.utility.getLoggedUser('lastname')}`,
      },
      theme: {
        color: '#3096f2'
      },
      modal: {
        ondismiss: function () {
          alert('dismissed');
        }
      }
    };
    const successCallback = (paymentId) => {
      this.cashpayment('razorpay', paymentId);
    };
    const cancelCallback = (error) => {
      alert(error.description + ' (Error ' + error.code + ')');
    };
    this.platform.ready().then(() => {
      RazorpayCheckout.open(options, successCallback, cancelCallback);
    });
  }
  cashpayment(type, paymentId) {
    const orderitem = [];
    this.checkoutList.forEach(element => {
      orderitem.push(element.id);
    });
    const obj = {
      userid: this.utility.getLoggedUser('_id'),
      totalprice: this.totalPrice,
      status: 0,
      discount: this.totalDiscount,
      delivered: 0,
      deliveredfee: this.deliveryfee,
      sgst: 0,
      cgst: 0,
      containercharges: 0,
      orderitem: orderitem,
      shopid: this.shop.id,
      orderType: (this.pickup) ? 'pickup' : 'delivery',
      instruction: (this.instruction) ? this.instruction : '',
      payment_recived: (type === 'razorpay') ? 1 : 0,
      payment_type: type,
      transactionid: (type === 'razorpay') ? paymentId : ''
    };
    this._services.createOrder(obj).subscribe((response: any) => {
      const id = response.response.data.id;
      this.navCtrl.setRoot('YourOrderPage', { id: id });
    });
  }
  paytm() {
    this.navCtrl.push('PaytmPage');
  }
  price(amount) {
    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'INR',
      minimumFractionDigits: 2,
    });
    return formatter.format(amount);
  }
  getShippingCharges() {
    const charges = JSON.parse(localStorage.getItem('shippingArea'));
    return charges.value;
  }
}
