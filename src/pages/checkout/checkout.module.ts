import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckoutPage } from './checkout';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { FormsModule } from '@angular/forms';
import { Clipboard } from '@ionic-native/clipboard';

@NgModule({
  declarations: [
    CheckoutPage,
  ],
  imports: [FormsModule,
    IonicPageModule.forChild(CheckoutPage),
  ],
  providers: [InAppBrowser, Clipboard]
})
export class CheckoutPageModule {}
