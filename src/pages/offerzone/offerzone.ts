import { Component, ViewChild } from '@angular/core';
import { IonicPage, Slides, NavController, NavParams, ModalController, Events, ActionSheetController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
import { Content } from 'ionic-angular';
/**
 * Generated class for the OfferzonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-offerzone',
  templateUrl: 'offerzone.html',
})
export class OfferzonePage {
  @ViewChild('pageTop') pageTop: Content;

  @ViewChild(Slides) slides: Slides;
  prodcuctList: any[];
  categoryid: any;
  qty: number;
  searchTerm = '';
  sort: number;
  productCount: any;
  prodcuctListData: any[];
  tag: any = [];
  offerItemList: any;
  offerDiscount: any;
  offerPrice: any;
  discountedValue: any;
  dicountedPrice: any;
  public showLeftButton: boolean;
  public showRightButton: boolean;
  selectedCategory: any = {};
  constructor(public utility: UtilityProvider, public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController, public events: Events,
    public _services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.events.subscribe('grocery', () => {
      this.moveToSlide('4');
    });
  }
  ionViewDidLoad() {
    this.events.publish('cartupdated', 'ionViewDidLoad offerzone');
    this._services.getTag().subscribe((response) => {
      const tag = response.response.data;
      this.tag = tag.filter(item => item.active === true);
      this.tag = this.tag.sort((a, b) => (b.tag < a.tag) ? 1 : -1);
      this.getOfferItemsList();
    });
  }
  getOfferItemsList() {
    let result = [];
    this._services.getOfferItemsList().subscribe((response) => {
      result = response.response.data;
      result.sort((a, b) => a.title.localeCompare(b.title));
      this.prodcuctList = result.map((element) => {
        element.dicountedPrice = element.price - element.discount;
        return element;
      });
    });
  }
  addToCart(item) {
    const modal = this.modalCtrl.create('QtyModalPage', { id: item });
    modal.present();
    modal.onDidDismiss(data => {
      this.events.publish('cartupdated', 'addToCart offerzone');
    });
  }
  goToCart() {
    this.navCtrl.push('CartPage');
  }
  filterTechnologies(param: any): void {
    this.prodcuctListData = this.prodcuctList;
    const val: string = param;
     if (val && val.trim() !== '') {
      this.prodcuctListData = this.prodcuctListData.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }
  postDetailsPage(item) {
    this.navCtrl.push('PostdetailsPage', { id: item.shop.id });
  }
  private initializeCategories(): void {
    this.selectedCategory = this.tag[0];
    this.showLeftButton = false;
    this.showRightButton = this.tag.length > 3;
    this.filterTag(0);
  }
  public slideChanged(event): void {
    const currentIndex = this.slides.getActiveIndex();
    this.filterTag(currentIndex);
    this.showLeftButton = currentIndex !== 0;
    this.showRightButton = currentIndex !== Math.ceil(this.slides.length() / 3);
  }
  public slideTap(event): void {
    this.slides.slideTo(this.slides.clickedIndex, 800);

  }
  public moveToSlide(index): void {
    this.slides.slideTo(this.slides[index], 800);
  }
  public slideNext(): void {
    this.slides.slideNext();
  }
  public slidePrev(): void {
    this.slides.slidePrev();
  }
  filterTag(currentIndex) {
    if (currentIndex === undefined) {
      return;
    }
    this.prodcuctListData = [];
    const currentItem = this.tag[currentIndex];
    this.prodcuctListData = this.prodcuctList.filter((item) => {
      this.pageTop.scrollToTop(800);

      if (item.tag) {
        return currentItem.tag === item.tag.tag;
      }
    });
  }
  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Filter by Price',
      buttons: [
        {
          text: 'High To Low',
          role: 'destructive',
          handler: () => {
            this.sort = 1;
          }
        },
        {
          text: 'Low To High',
          handler: () => {
            this.sort = 2;
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }
  backButton() {
    this.navCtrl.parent.select(0);
  }
}
