import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OfferzonePage } from './offerzone';
import { SharedModule } from '../../shared/shared.module';
@NgModule({
  declarations: [
    OfferzonePage,
  ],
  imports: [SharedModule,
    IonicPageModule.forChild(OfferzonePage),
  ],
})
export class OfferzonePageModule { }
