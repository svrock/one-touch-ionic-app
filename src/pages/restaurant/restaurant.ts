import { Component, Injector, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { IonicPage, Slides, NavController, NavParams, ModalController, Item } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
import { Subject } from 'rxjs/Subject';
import { Content } from 'ionic-angular';
import { Events } from 'ionic-angular';


/**
* Generated class for the MyshopPage page.
*
* See https:
* Ionic pages and navigation.
*/
@Component({
  selector: 'page-restaurant',
  templateUrl: 'restaurant.html',
})
export class RestaurantPage {
  @ViewChild('pageTop') pageTop: Content;
  @ViewChild(Slides) slides: Slides;
  parentSubject: Subject<any> = new Subject();
  public showLeftButton: boolean;
  public showRightButton: boolean;
  postList = [];
  postListItem = [];
  category: any = [];
  image: any;
  tagsegment: string;
  resultResponse: any = [];
  product: any = [];
  public searchText: String = '';
  valuesToRemove = ['5eecee16857510257e4fb255', '5ef021ad4e18f628dc4392bf'];
  constructor(public utility: UtilityProvider,
    public modalCtrl: ModalController,
    public _services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams, public events: Events) {
  }
  ionViewDidLoad() {
    this._services.getShopProduct().subscribe((response) => {
      this.resultResponse = response.response.data;
      let result = response.response.data;
      result = result.filter((item) => item.hasOwnProperty('shop'));
      const shopList = result.map((item) => item.shop);
      const filteredShop = this.removeDuplicate(shopList, 'id');
      this.postList = filteredShop.filter(item => !this.valuesToRemove.includes(item.id));
      this.postListItem = this.postList;
      this.getProductCategory(this.postList[0].id);

    });
  }
  ionViewDidEnter() {
    this.searchText = '';
    this.postList = this.postListItem;

  }
  public slideChanged(event): void {
    const currentIndex = this.slides.getActiveIndex();
    this.getProductCategory(this.postList[currentIndex].id);
    this.showLeftButton = currentIndex !== 0;
    this.showRightButton = currentIndex !== Math.ceil(this.slides.length() / 3);
  }
  public slideTap(event): void {
    this.slides.slideTo(this.slides.clickedIndex, 800);
  }
  public slideNext(): void {
    this.slides.slideNext();
  }
  public slidePrev(): void {
    this.slides.slidePrev();
  }
  removeDuplicate(result, key) {
    return result.reduce((acc, current) => {
      const x = acc.find(item => item[key] === current[key]);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);
  }
  getProductCategory(id) {
    const d = this.resultResponse;
    const a = d.filter((item) => item.product_category.shopid === id).map((item, i) => {
      item.product_category.value = i + 1;
      this.pageTop.scrollToTop(800);

      return item.product_category;
    }
    );
    this.category = this.removeDuplicate(a, 'id');
    this.category = this.category.sort((_a, b) => {
      return b.title - b.title;
    });
    this.setSegmentActive(this.category[0].id);
  }
  setSegmentActive(id) {
    this.tagsegment = id;
    const d = this.resultResponse;
    this.product = d.filter((item) => item.product_category.id === id);
    this.parentSubject.next(id);

  }
  public getUserInputForSearch(event) {
    this.postList = this.postListItem;
    const userSearchData = event.target.value;

    if (userSearchData && userSearchData.trim() !== '') {
      this.postList = this.postList.filter((postList) => {
        return postList.title.toLowerCase().indexOf(userSearchData.toLowerCase()) > -1;
      });
    }
    setTimeout(() => {
      if (this.postList[0] && (this.postList[0].title === userSearchData)) {
        const test = this.postListItem.findIndex((item) => item.title === userSearchData);
      }
    }, 1000);

  }
}
