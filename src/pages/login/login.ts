import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController, MenuController, Events } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { config } from '../../config';
import { LoadingController } from 'ionic-angular';
/**
 *
* Generated class for the LoginPage page.
*
* See https:
* Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user: any = {};
  loginPage = true;
  userdata: any;
  loadingProgress: any;
  verifedPhone: string;
  passwordType = 'password';
  passwordIcon = 'eye-off';
  constructor(public utility: UtilityProvider,
    public loadingCtrl: LoadingController,
    private fb: Facebook, public events: Events,
    public alertCtrl: AlertController,
    public navCtrl: NavController, private platform: Platform,
    public navParams: NavParams,
    public menu: MenuController,
    public _services: ServicesProvider) {
  }
  ngOnInit() {
    this.menu.enable(false);
  }
  gotoregister() {
    this.loginPage = false;
  }
  gotologin() {
    this.loginPage = true;
  }
  forgetPassword() {
    const prompt = this.alertCtrl.create({
      title: 'Forget Password',
      message: 'Enter Email',
      inputs: [
        {
          name: 'email',
          placeholder: 'Email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
          }
        },
        {
          text: 'Send',
          handler: data => {
            if (!data.email) {
              const msg = { msg: 'Please enter your register email', duration: 3000, position: 'centre' };
              this.utility.showToast(msg);
              return false;
            }
            this._services.forgetPassword(data).subscribe((response: any) => {
              if (response === null) {
                const msg = { msg: 'Please check you email and follow the instruction', duration: 3000, position: 'centre' };
                this.utility.showToast(msg);
              } else {
                const msg = { msg: response.message, duration: 3000, position: 'centre' };
                this.utility.showToast(msg);
              }
            });
          }
        }
      ]
    });
    prompt.present();
  }

  hideShowPassword() {

    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  isPasswordCommon(userPassword) {
    const commonPasswordLength = config.commonPassword.length;
    for (let i = 0; i < commonPasswordLength; i++) {
      if (config.commonPassword[i] === userPassword) {
        return true;
      }
    }
    return false;
  }
  register() {

    // tslint:disable-next-line: max-line-length
    const emailExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!this.user.firstname) {
      const msg = { msg: 'First name is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (!this.user.lastname) {
      const msg = { msg: 'Last name is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (!this.user.email) {
      const msg = { msg: 'Email is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (emailExp.test(this.user.email) === false) {
      const msg = { msg: 'Enter valid email ', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (!this.user.phone) {
      const msg = { msg: 'Enter phone number for order tracking', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (this.user.phone.length < 10) {
      const msg = { msg: 'Phone number is not valid', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (!this.user.password) {
      const msg = { msg: 'Password is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (this.isPasswordCommon(this.user.password)) {
      const msg = { msg: 'This is two week password .Please enter a strong password', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (this.user.password.length < 5) {
      const msg = { msg: 'Password atleast 6 character required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }

    const obj = {
      firstname: this.user.firstname.trim(),
      lastname: this.user.lastname.trim(),
      email: this.user.email.trim(),
      password: this.user.password.trim(),
      phone: this.user.phone.trim(),
      type: 'customer',
    };
    this._services.register(obj).subscribe((response: any) => {
      if (response.success === true) {
        const msg = { msg: 'Successful register', duration: 3000, position: 'centre' };
        this.utility.messageToast(msg);
        this.login();
      } else {
        const msg = { msg: response.message, duration: 3000, position: 'centre' };
        this.utility.messageToast(msg);
      }
    });
  }
  login() {
    if (!this.user.email) {
      const msg = { msg: 'Email is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }

    if (!this.user.password) {
      const msg = { msg: 'Password is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    this._services.doLogin(this.user).subscribe((response: any) => {
      if (response.success === true) {
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('userdetails', JSON.stringify(response.data.userDetails));
        this.redirectAfterLogin();
      } else {
        const msg = {
          msg: response.message,
          duration: 3000,
          position: 'centre',
        };
        this.utility.messageToast(msg);
      }
    });
  }
  redirectAfterLogin() {
    this.navCtrl.setRoot('WelcomePage');
    this.events.publish('userdetails');
  }
  fbook() {
    this.fb.login(['public_profile', 'email'])
      .then((res: FacebookLoginResponse) => {
        this._services.getFacebookGraphApi(res).subscribe((response: any) => {
          if (!response.first_name || !response.last_name || !response.email) {
            const msg = { msg: 'Facebook Login failed Please login through email or register now ', duration: 3000, position: 'centre' };
            this.utility.showToast(msg);
            return;
          }
          const obj = {
            firstname: response.first_name,
            lastname: response.last_name,
            email: response.email,
            about: response.about,
            profile_image: response.picture.data.url,
            birthday: response.birthday,
            facebook_id: response.id,
            type: 'customer',

          };
          this.user = {
            imageUrl: obj.profile_image,
            name: obj.firstname + ' ' + obj.lastname,
          };
          this.fbLogin(obj);
        });
      });
  }
  fbLogin(data) {
    this._services.login(data).subscribe((response: any) => {
      if (response.success) {
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('userdetails', JSON.stringify(response.data.userDetails));
        this.redirectAfterLogin();
      }
    });
  }
}
