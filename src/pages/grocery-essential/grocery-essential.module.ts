import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroceryEssentialPage } from './grocery-essential';

@NgModule({
  declarations: [
    GroceryEssentialPage,
  ],
  imports: [
    IonicPageModule.forChild(GroceryEssentialPage),
  ],
})
export class GroceryEssentialPageModule {}
