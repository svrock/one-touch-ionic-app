import { Component, Injector, ViewChild, Input } from '@angular/core';
import { IonicPage, Slides, NavController, NavParams, ModalController, Events, ActionSheetController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
import { Subject } from 'rxjs/Subject';
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
  @Input()
  parentSubject: Subject<any>;
  categoryid: any;
  prodcuctList: any = [];
  filteredProductList: any = [];
  qty: number;
  sort: number;
  constructor(public utility: UtilityProvider, public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController, public events: Events,
    public _services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.categoryid = this.navParams.data.id;
  }
  ngOnDestroy() {
    this.parentSubject.unsubscribe();
  }
  ngOnInit() {
    let result = [];
    this.events.publish('cartupdated', 'ngOnInit prodcuct');
    this.parentSubject.subscribe(data => {
      this._services.getProduct(data).subscribe((response: any) => {
        result = response.response.data;
        result.sort((a, b) => a.title.localeCompare(b.title));
        this.prodcuctList = result;
        this.sort = 2;
        this.filteredProductList = result.filter(function(e) {
          return e.active === true;
        });
      });
    });
    this.countCartItem();
  }
  ionViewDidEnter() {
    this.countCartItem();
  }
  addToCart(item) {


    const modal = this.modalCtrl.create('QtyModalPage', { id: item });
    modal.onDidDismiss(data => {
      this.events.publish('cartupdated', 'ngOnInit prodcuct');
    });
    modal.present();
  }
  countCartItem() {
    const obj = {
      id: this.utility.getLoggedUser('_id')
    };
    this._services.getCart(obj).subscribe((response: any) => {
      this.qty = 0;
      const result = response.response.data;
      const pro = [];
      result.filter((item) => item.shopid).forEach(element => {
        this.qty += element.quantity;
      });
    }, (error) => {
      this.qty = 0;
    });
  }
  goToCart() {
    if (this.qty < 1) {
      const msg = {
        msg: `Please add some item to your product`,
        duration: 3000
      };
      this.utility.showToast(msg);
      return false;
    }
    this.navCtrl.push('CartPage');
  }

}
