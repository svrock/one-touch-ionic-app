import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';

@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {
  feed: any = {};
  constructor(
    public utility: UtilityProvider, public navCtrl: NavController,
    public navParams: NavParams, public _services: ServicesProvider) {
  }
  ionViewDidLoad() {
  }
  submit() {
    if (!this.feed.subject) {
      const msg = { msg: 'Subject is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (!this.feed.comment) {
      const msg = { msg: 'Comment is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    this._services.feedback(this.feed).subscribe((response: any) => {
      if (response.success === true) {
        this.navCtrl.pop();
        const msg = { msg: 'Successful sent', duration: 3000, position: 'centre' };
        this.utility.messageToast(msg);
      } else {
        const msg = { msg: response.message, duration: 3000, position: 'centre' };
        this.utility.messageToast(msg);
      }
    });
  }
}
