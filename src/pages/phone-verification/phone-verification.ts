import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
@IonicPage()
@Component({
  selector: 'page-phone-verification',
  templateUrl: 'phone-verification.html',
})
export class PhoneVerificationPage {
  phone: any;
  showotp = true;
  otp: any;
  constructor(private _ngZone: NgZone, public platform: Platform, public viewCtrl: ViewController,
    public utility: UtilityProvider, public navCtrl: NavController,
    public navParams: NavParams, public _services: ServicesProvider) {
  }
  ionViewDidLoad() {
  }
  sendOTP() {
    const phoneno = /^\d{10}$/;
    if (phoneno.test(this.phone) === false) {
      const msg = { msg: 'Phone number is not valid', duration: 3000, position: 'centre' };
      this.utility.showToast(msg);
      return false;
    }
    const obj = {
      phone: this.phone,
      email: this.utility.getLoggedUser('email')
    };
    this._services.sendOTP(obj).subscribe((response: any) => {
      const data = JSON.parse(response.response.data);
      if (data.type === 'success') {
        this.showotp = false;
        this.watchOTP();
      }
    });
  }
  list() {

  }
  clear() {
    this.phone = '';
  }
  watchOTP() {

  }
  verifyotp() {
    const obj = {
      phone: this.phone,
      otp: this.otp
    };
    this._services.verifyOTP(obj).subscribe((response: any) => {
      const data = JSON.parse(response.response.data);
      if (data.type === 'success') {
        const msg = {
          msg: 'Verified',
          duration: 3000
        };
        this.utility.showToast(msg);
        this.viewCtrl.dismiss(this.phone);
      } else {
        const msg = {
          msg: 'Wrong OTP',
          duration: 3000
        };
        this.utility.showToast(msg);
        this.viewCtrl.dismiss(this.phone);
      }
    });
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
