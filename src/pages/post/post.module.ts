import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostPage } from './post';
import { RatingModule } from 'ngx-rating';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    PostPage,
  ],
  imports: [RatingModule, SharedModule,
    IonicPageModule.forChild(PostPage),
  ],
})
export class PostPageModule {}
