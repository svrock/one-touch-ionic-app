import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';

/**
* Generated class for the PostPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
@IonicPage({
  segment: 'page-post/id/:id/title/:title'
})
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {
  postList: any = [];
  post: any = {};
  title: any;
  image: any;
  constructor(public utility: UtilityProvider, public navCtrl: NavController,
    public navParams: NavParams, public _services: ServicesProvider) {
    this.post = this.navParams.data.id;
    this.title = this.navParams.data.title;
    this.image = this.navParams.data.image;
  }
  ionViewDidLoad() {
    const obj = {
      type: 'category',
      catid: this.post,
      region: localStorage.getItem('region')
    };
    this._services.search(obj).subscribe((response: any) => {
      const result = response.data;
      const list = [];
      result.forEach(data => {
        const element = {};
        const resultdata = data.result;
        element['images'] = (resultdata.images.length < 1 || !resultdata.images) ? this.image : resultdata.images;
        element['prodict_available'] = data.category;
        element['title'] = resultdata.title;
        element['address'] = resultdata.address;
        element['_id'] = resultdata._id;
        element['rating'] = resultdata.rating;
        list.push(element);
      });
      list.sort(function (a, b) {
        return b.prodict_available - a.prodict_available;
      });

      this.postList = list;
    }, () => {
    });
  }
  PostdetailsPage(item) {
    this.navCtrl.push('PostdetailsPage', { id: item });
  }
}
