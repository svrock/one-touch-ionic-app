import { Component } from '@angular/core';
import { IonicPage, Events } from 'ionic-angular';
import { HomePage } from '../home/home';
import { MyshopPage } from '../myshop/myshop';
import { ServicesProvider } from '../../providers/services/services';
import { RestaurantPage } from '../restaurant/restaurant';
import { CartPage } from '../cart/cart';
import { UtilityProvider } from '../../providers/utility/utility';
import { DoctorbookingPage } from '../doctorbooking/doctorbooking';

/**
 * Generated class for the WelcomePage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {
  qty: number;
  foodRoot = MyshopPage;
  doctorRoot = DoctorbookingPage;
  homeRoot = HomePage;
  recommendedRoot = RestaurantPage;
  cartRoot = CartPage;
  constructor(public _services: ServicesProvider, public events: Events, public utility: UtilityProvider, ) {
    this.events.subscribe('cartupdated', (data) => {
      this.countCartItem();
    });
  }
  ionViewDidLoad() {
    this.countCartItem();
    this.getShopProduct();
  }
  getShopProduct() {
    this._services.getShopProduct().subscribe((response) => {
    });
  }
  countCartItem() {
    const obj = {
      id: this.utility.getLoggedUser('_id')
    };
    this._services.getCart(obj).subscribe((response: any) => {
      this.qty = 0; const pro = [];
      const result = response.response.data;
      result.filter((item) => item.shopid).forEach(element => {
        this.qty += element.quantity;
      });
    }, (error) => {
      this.qty = 0;
    });
  }
  ngOnDestroy() {
    this.events.unsubscribe('cartupdated');
  }
}
