import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WelcomePage } from './welcome';
 import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    WelcomePage,
  ],

  imports: [SharedModule,
    IonicPageModule.forChild(WelcomePage),
  ]
})
export class WelcomePageModule { }
