import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
/**
* Generated class for the MyOrderPage page.
*
* See https:
* Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-my-order',
  templateUrl: 'my-order.html',
})
export class MyOrderPage {
  loggedInid: any;
  order: any[];
  productList: any[];
  constructor(
    public utility: UtilityProvider, public navCtrl: NavController,
    public navParams: NavParams, public _services: ServicesProvider) {
    this.loggedInid = this.utility.getLoggedUser('_id');
  }
  ionViewDidLoad() {
    this._services.getOrderList(this.loggedInid).subscribe((response: any) => {
      this.productList = response.response.data;
    }, (error) => {
    });
  }
  orderDetails(item) {
    this.navCtrl.push('YourOrderPage', { id: item.id });
  }
}
