import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
/**
 * Generated class for the VendorjoinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-vendorjoin',
  templateUrl: 'vendorjoin.html',
})
export class VendorjoinPage {
  user: any = {};
  constructor(public utility: UtilityProvider,
    public navCtrl: NavController, public navParams: NavParams, public _services: ServicesProvider) {
  }
  ionViewDidLoad() {
  }
  submit() {
    const phoneExp = /^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/;
    if (!this.user.shopname) {
      const msg = { msg: 'Shop name is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (!this.user.phone) {
      const msg = { msg: 'Phone number is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (!phoneExp.test(this.user.phone)) {
      const msg = { msg: 'Enter valid phone number', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (!this.user.address) {
      const msg = { msg: 'Address is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (!this.user.about) {
      const msg = { msg: 'About is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    this.user.id = this.utility.getLoggedUser('_id');
    this._services.sendRequestVendorJoin(this.user).subscribe((response) => {
      this.navCtrl.pop();
      const msg = { msg: 'Thankyou for your interest we will contact you soon', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
    });
  }
}
