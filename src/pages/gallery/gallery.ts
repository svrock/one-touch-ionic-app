import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ModalController} from 'ionic-angular';
import { GalleryModal } from 'ionic-gallery-modal';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
/**
* Generated class for the GalleryPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
@IonicPage({
  segment: 'page-gallery/id/:id/type/:type'
})
@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html',
})
export class GalleryPage {
  galleryType = 'regular';
  promoterList: any = [];
  constructor(public navCtrl: NavController, public modalCtrl: ModalController,
    public _services: ServicesProvider, public toast: UtilityProvider,
    public navParams: NavParams) {
    }
    ionViewDidLoad() {
      const obj = {
        id: this.navParams.data.id
      };
      this._services.getPromoterImage(obj).subscribe((response: any) => {
        this.promoterList = response.response.data;
      });
    }
    openZoom(image) {
      const galleryimage = [];
      this.promoterList.forEach(element => {
        const val = {};
        val['url'] =  element.image;
        galleryimage.push(val);
      });
      const modal = this.modalCtrl.create(GalleryModal, {
        photos: galleryimage,
        initialSlide: 0
      });
      modal.present();
    }
  }
