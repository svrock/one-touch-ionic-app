import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantSearchbarPage } from './restaurant-searchbar';

@NgModule({
  declarations: [
    RestaurantSearchbarPage,
  ],
  imports: [
    IonicPageModule.forChild(RestaurantSearchbarPage),
  ],
})
export class RestaurantSearchbarPageModule {}
