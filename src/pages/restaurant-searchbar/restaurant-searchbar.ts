import { Component, ViewChild } from '@angular/core';
import { IonicPage, Slides, NavController, NavParams, ModalController, Content, Events } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
import { Subject } from 'rxjs/Subject';

/**
 * Generated class for the RestaurantSearchbarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-restaurant-searchbar',
  templateUrl: 'restaurant-searchbar.html',
})
export class RestaurantSearchbarPage {
  @ViewChild('pageTop') pageTop: Content;
  @ViewChild(Slides) slides: Slides;
  parentSubject: Subject<any> = new Subject();
  postList = [];
  postListItem = [];
  resultResponse: any = [];
  category: any = [];
  tagsegment: string;
  product: any = [];
  valuesToRemove = ['5eecee16857510257e4fb255', '5ef021ad4e18f628dc4392bf'];
  constructor(public utility: UtilityProvider,
    public modalCtrl: ModalController,
    public _services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams, public events: Events) {
  }
  ionViewDidLoad() {
    this._services.getShopProduct().subscribe((response) => {
      this.resultResponse = response.response.data;
      let result = response.response.data;
      result = result.filter((item) => item.hasOwnProperty('shop'));
      const shopList = result.filter((item) => (item.shop !== null));
      const filtershopList = shopList.map((item) => {
        return {
          active: item.shop.active,
          id: item.shop.id,
          title: item.shop.title,
          image: item.image,
        };
      });
      const filteredShop = this.removeDuplicate(filtershopList, 'id');
      this.postList = filteredShop.filter(item => !this.valuesToRemove.includes(item.id));
      this.postListItem = this.postList;
    });
  }
  public getUserInputForSearch(event) {
    this.postList = this.postListItem;
    const userSearchData = event.target.value;
    if (userSearchData && userSearchData.trim() !== '') {
      this.postList = this.postList.filter((postList) => {
        return postList.title.toLowerCase().indexOf(userSearchData.toLowerCase()) > -1;
      });
    }
    setTimeout(() => {
      if ( this.postList[0] && (this.postList[0].title === userSearchData )) {
        const test = this.postListItem.findIndex((item) => item.title === userSearchData);
      }
    }, 1000);
  }
  removeDuplicate(result, key) {
    return result.reduce((acc, current) => {
      const x = acc.find(item => item[key] === current[key]);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);
  }
  openRestaurantDetailsPage(item) {
    this.navCtrl.setRoot('RestaurantdetailsPage', { id: item });
  }
  backButton() {
    this.navCtrl.parent.select(0);
  }
}
