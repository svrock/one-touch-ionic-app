import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { config } from '../../config';
import { DomSanitizer } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the DoctorbookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-doctorbooking',
  templateUrl: 'doctorbooking.html',
})
export class DoctorbookingPage {
  url: any;
  constructor(private iab: InAppBrowser,
    public sanitizer: DomSanitizer, public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewDidEnter() {

    const userToken = localStorage.getItem('token');
    const urlToken = `${config.doctorBooking}?token=${userToken}`;
     this.url = this.getSantizedURL(urlToken);
    // const browser = this.iab.create(urlToken, '_self', {
    //   location: 'no',
    //   clearcache: 'yes',
    //   toolbar: 'yes',
    //   closebuttoncaption: 'Close',
    // });

    // browser.on('close').subscribe(event => {
    //   alert('close');
    // });
  }
  openMenu() {
    this.navCtrl.popToRoot();
    this.navCtrl.parent.select(0);
  }
  getSantizedURL(mySampleURL) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(mySampleURL);
  }
}
