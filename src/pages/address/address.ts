import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { UtilityProvider } from '../../providers/utility/utility';
import { ServicesProvider } from '../../providers/services/services';
@IonicPage()
@Component({
  selector: 'page-address',
  templateUrl: 'address.html',
})
export class AddressPage {
  add: any = {};
  constructor(
    public utility: UtilityProvider, public navCtrl: NavController, public modalCtrl: ModalController,
    public navParams: NavParams, public _services: ServicesProvider) {
  }
  ionViewDidLoad() {
    const data = {
      userid: this.utility.getLoggedUser('_id')
    };
    this._services.getShippingAddress(data).subscribe((response: any) => {
      const result = response.response.data;
      if (result.length > 0) {
        this.add = response.response.data[0];
      } else {
        this.add.phone = this.utility.getLoggedUser('phone');
      }
    });
  }
  saveAddress() {
    if (!this.add.phone) {
      const msg = { msg: 'Phone is required', duration: 3000, position: 'centre' };
      this.utility.showToast(msg);
      return false;
    }
    if (!this.add.city) {
      const msg = { msg: 'City is required', duration: 3000, position: 'centre' };
      this.utility.showToast(msg);
      return false;
    }
    if (!this.add.locality) {
      const msg = { msg: 'Locality is required', duration: 3000, position: 'centre' };
      this.utility.showToast(msg);
      return false;
    }
    if (!this.add.address) {
      const msg = { msg: 'Address is required', duration: 3000, position: 'centre' };
      this.utility.showToast(msg);
      return false;
    }
    if (!this.add.landmark) {
      const msg = { msg: 'Landmark is required', duration: 3000, position: 'centre' };
      this.utility.showToast(msg);
      return false;
    }
    this.add.userid = this.utility.getLoggedUser('_id');
    this._services.shippingAddress(this.add).subscribe((response: any) => {
      const msg = { msg: 'Successful save', duration: 3000, position: 'centre' };
      this.utility.showToast(msg);
      this.navCtrl.push('CheckoutPage');
    });
  }
  changePhoneNumber() {
    const modal = this.modalCtrl.create('PhoneVerificationPage');
    modal.present();
    modal.onDidDismiss(data => {
      this.add.phone = data;
    });
  }
}
