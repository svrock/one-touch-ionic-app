import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
/**
 * Generated class for the VendorPage page.
 *
 * See https:
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-vendor',
  templateUrl: 'vendor.html',
})
export class VendorPage {
  vendorList: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public _services: ServicesProvider) {
  }
  ionViewDidLoad() {
    this._services.getVendor().subscribe((response: any) => {
      this.vendorList = response.data;
    }, (error) => {
    });
  }
  goToProfile(item) {
    this.navCtrl.push('ProfilePage', { data: item });
  }
}
