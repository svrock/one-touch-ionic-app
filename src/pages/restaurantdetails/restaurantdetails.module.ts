import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RestaurantdetailsPage } from './restaurantdetails';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    RestaurantdetailsPage,
  ],
  imports: [SharedModule,
    IonicPageModule.forChild(RestaurantdetailsPage),
  ],
  entryComponents: [
  ],
})
export class RestaurantdetailsPageModule { }
