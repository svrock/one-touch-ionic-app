import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, ModalController, Events } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { config } from '../../config';
import { UtilityProvider } from '../../providers/utility/utility';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the RestaurantdetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  segment: 'page-restaurantdetails/:id'
})
@Component({
  selector: 'page-restaurantdetails',
  templateUrl: 'restaurantdetails.html',
})
export class RestaurantdetailsPage {
  @ViewChild(Slides) slides: Slides;
  post: any;
  postdetails: any = {};
  category: any = {};
  userdetails: any = {};
  imagedetails: any = [];
  productCategoryList: any = [];
  ratingshow: number;
  productListByRest: any = [];

  constructor(private iab: InAppBrowser, public utility: UtilityProvider,
    public navCtrl: NavController, public navParams: NavParams,
    public _services: ServicesProvider, public modalCtrl: ModalController, public events: Events) {
    this.post = this.navParams.data.id;
  }

  ngOnInit() {
    this.events.publish('cartupdated', 'ngOnInit restaurantdetails');
    this._services.getPostDetails(this.post).subscribe((response: any) => {
      this.postdetails = response.data.post;
      this.userdetails = response.data.user;
      this.category = response.data.category;
      const image = response.data.image;
      if (image && image.length < 1) {
        const res = {};
        res['image'] = 'assets/imgs/placeholder.png';
        this.imagedetails.push(res);
        return false;
      }
      const imageval = [];
      image.forEach(element => {
        const res = {};
        res['image'] = `${config.SOCKET_ENDPOINT}image/postimage/${element.image}`;
        imageval.push(res);
      });
      this.imagedetails = imageval;
    });
    this.getProductbyResturant(this.post);
  }
  getProductCategory() {
    const obj = {
      shopid: this.post
    };
    this._services.getProductCategory(obj).subscribe((response: any) => {
      this.productCategoryList = response.response.data;
    }, (error) => {
    });
  }
  openWeb(url) {
    this.iab.create(url);
  }
  getWhatsAppMessage(item) {
    return `https://wa.me/${config.agentNumber}?text=${config.POST_CUSTOM_MESSAGE} ${item.title} ${item.address}`;
  }
  getProductbyResturant(shopid) {
    this._services.getProductbyResturant(shopid).subscribe((response: any) => {
      this.productListByRest = response.response.data;
    });
  }
  addToCart(item) {
    const obj = {
      discount: 0,
      id: item.id,
      image: item.image,
      offerprice: item.offerprice,
      price: item.price,
      product_category: {
        id: this.post,
        shopid: this.post
      },
      title: item.title
    };
    const modal = this.modalCtrl.create('QtyModalPage', { id: obj });
    modal.present();
    modal.onDidDismiss(data => {
      this.events.publish('cartupdated', 'addToCart offerzone');
    });
  }
  backButton() {
    this.navCtrl.parent.select(0);
  }
}
