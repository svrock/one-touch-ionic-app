import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GhostElementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ghost-element',
  templateUrl: 'ghost-element.html',
})
export class GhostElementPage {
  fakeItem = [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.fakeItem = new Array(5);

  }

  ionViewDidLoad() {

  }

}
