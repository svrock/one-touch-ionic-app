import { Component, ViewChild } from '@angular/core';
import { IonicPage, Slides, NavController, NavParams, ModalController, Content, Events } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
import { Subject } from 'rxjs/Subject';

/**
 * Generated class for the MeatSearchbarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-meat-searchbar',
  templateUrl: 'meat-searchbar.html',
})
export class MeatSearchbarPage {
  @ViewChild('pageTop') pageTop: Content;
  @ViewChild(Slides) slides: Slides;
  parentSubject: Subject<any> = new Subject();
  postList = [];
  postListItem = [];
  private readonly MEAT_SHOP_ID = '5ee1169a65eff46f176abda5';
  constructor(public utility: UtilityProvider,
    public modalCtrl: ModalController,
    public _services: ServicesProvider, public navCtrl: NavController, public navParams: NavParams, public events: Events) {
  }
  ionViewDidLoad() {
    const data = {
      id: this.MEAT_SHOP_ID
    };
    this._services.getPostForMeatCat(data).toPromise().then((Response: any) => {
      this.postList = Response.data;
      this.postListItem = this.postList;
    }).catch((err) => {});
  }
  public getUserInputForSearch(event) {
     this.postList = this.postListItem;
    const userSearchData = event.target.value;
    if (userSearchData && userSearchData.trim() !== '') {
      this.postList = this.postList.filter((postList) => {
        return postList.title.toLowerCase().indexOf(userSearchData.toLowerCase()) > -1;
      });
    }
  }
  removeDuplicate(result, key) {
    return result.reduce((acc, current) => {
      const x = acc.find(item => item[key] === current[key]);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);
  }
  openRestaurantDetailsPage(item) {
    this.navCtrl.setRoot('RestaurantdetailsPage', { id: item });
  }
  backButton() {
    this.navCtrl.parent.select(0);
  }
}
