import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeatSearchbarPage } from './meat-searchbar';

@NgModule({
  declarations: [
    MeatSearchbarPage,
  ],
  imports: [
    IonicPageModule.forChild(MeatSearchbarPage),
  ],
})
export class MeatSearchbarPageModule {}
