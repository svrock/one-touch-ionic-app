import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { UtilityProvider } from '../../providers/utility/utility';
import { ServicesProvider } from '../../providers/services/services';
/**
 * Generated class for the LocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-location',
  templateUrl: 'location.html',
})
export class LocationPage {
  locations: any = [];
  constructor(
    public utility: UtilityProvider, public navCtrl: NavController, public modalCtrl: ModalController,
    public navParams: NavParams, public _services: ServicesProvider) {
  }
  ionViewDidLoad() {
    this._services.getLocation().subscribe((response) => {
      const result = response.data;
      this.locations = result;
    });
  }
  itemSelected(item) {
    localStorage.setItem('region', item._id);
    localStorage.setItem('regionSelected', item.title);
    const logincheck = localStorage.getItem('token');
    if (logincheck === null) {
      this.navCtrl.setRoot('LoginPage');
    } else {
      this.navCtrl.setRoot('WelcomePage');
    }
  }
  doRefresh(refresher) {
    this.ionViewDidLoad();
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }
}
