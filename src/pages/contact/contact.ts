import { Component } from '@angular/core';
import { NavController, IonicPage, ActionSheetController, AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
import { Camera } from '@ionic-native/camera';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  enableNotifications = true;
  defaultImage = 'assets/imgs/avatar.png';
  offset = 1000;
  userdata: any = {};
  constructor(public services: ServicesProvider, public alertCtrl: AlertController, public actionSheetCtrl: ActionSheetController,
    public utility: UtilityProvider,
    public navCtrl: NavController, private camera: Camera,
    // tslint:disable-next-line: deprecation
    private transfer: FileTransfer, private file: File
  ) { }
  ionViewDidEnter() {
    this.userdata = JSON.parse(localStorage.getItem('userdetails'));
  }
  doRefresh(refresher) {
    this.userdata = JSON.parse(localStorage.getItem('userdetails'));
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }
  join() {
    this.navCtrl.push('VendorjoinPage');
  }
  saveProfileEdit() {
    if (!this.userdata.phone) {
      const msg = { msg: 'phone is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (!this.userdata.zip) {
      const msg = { msg: 'zip is required', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    if (!this.userdata.about) {
      const msg = { msg: 'let us know your interest in about section', duration: 3000, position: 'centre' };
      this.utility.messageToast(msg);
      return false;
    }
    this.services.updateuser(this.userdata).subscribe((response) => {
      const msg = { msg: 'saved ', duration: 3000, position: 'centre' };
      localStorage.setItem('userdetails', JSON.stringify(this.userdata));
      this.utility.messageToast(msg);
      this.doRefresh('refresher');
    });
  }
  changePassword() {
    const prompt = this.alertCtrl.create({
      title: 'change Password',
      message: 'Enter Password',
      inputs: [
        {
          name: 'password',
          placeholder: 'password'
        },
        {
          name: 'cpassword',
          placeholder: 'Confirm password'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
          }
        },
        {
          text: 'Send',
          handler: data => {
            if (!data.password || !data.cpassword) {
              const msg = { msg: 'Please enter your password and confirm password', duration: 3000, position: 'centre' };
              this.utility.showToast(msg);
              return false;
            }
            if (data.password.length < 5) {
              const msg = { msg: 'Please enter at least 6 length password', duration: 3000, position: 'centre' };
              this.utility.showToast(msg);
              return false;
            }
            if (data.password !== data.cpassword) {
              const msg = { msg: 'Password and confirm password not match', duration: 3000, position: 'centre' };
              this.utility.showToast(msg);
              return false;
            }
            this.services.changePassword(data).subscribe((response: any) => {
              if (response === null) {
                const msg = { msg: 'Password Changed', duration: 3000, position: 'centre' };
                this.utility.showToast(msg);
              } else {
                const msg = { msg: response.message, duration: 3000, position: 'centre' };
                this.utility.showToast(msg);
              }
            });
          }
        }
      ]
    });
    prompt.present();
  }
  async updateProfileImage() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Choose',
      buttons: [
        {
          text: 'Camera',
          handler: () => {
            this.utility.opencamera('camera');
          }
        },
        {
          text: 'Gallery',
          handler: () => {
            this.utility.opencamera('file');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    actionSheet.present();
  }
}
