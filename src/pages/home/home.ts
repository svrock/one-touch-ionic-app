import { Component, ViewChild } from '@angular/core';
import { ToastController } from 'ionic-angular';
import {
  NavController, Slides, Platform, NavParams, MenuController, ModalController, ViewController, Events
} from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('slider') slider: Slides;
  categoryList = [];
  banner = [];
  notificationList: any;
  bannerone: any = [];
  regionSelected: any;
  constructor(public menuCtrl: MenuController, public events: Events, public platform: Platform,
    public utility: UtilityProvider, public navCtrl: NavController,
    public navParams: NavParams, public _services: ServicesProvider,
    public viewCtrl: ViewController, public modalCtrl: ModalController, private toastCtrl: ToastController) {
    if (this.platform.is('cordova')) {
      this.saveDevicesToken();
    }
  }
  ngOnInit(): void {
    this.menuCtrl.enable(true);
    this._services.getCategory().subscribe((response) => {
      this.categoryList = response.data;
    }, (error) => {
    });
    this.regionSelected = localStorage.getItem('regionSelected');
    if (!this.regionSelected) {
      this.navCtrl.setRoot('LocationPage');
    }
    this._services.getBanner().subscribe((response: any) => {
      const result = response.response.data;
      const banner1 = [];
      const banner2 = [];
      const banner3 = [];
      result.forEach(element => {
        const val = {};
        if (element.bannerindex === 1) {
          val['image'] = element.image;
          val['bannerindex'] = element.bannerindex;
          val['linkto'] = element.linkto;
          banner1.push(val);
        }
        if (element.bannerindex === 2) {
          val['image'] = element.image;
          val['bannerindex'] = element.bannerindex;
          banner2.push(val);
        }
        if (element.bannerindex === 3) {
          val['image'] = element.image;
          val['bannerindex'] = element.bannerindex;
          banner3.push(val);
        }
      });
      this.banner.push(banner1);
      this.banner.push(banner2);
      this.banner.push(banner3);
    });
  }
  ionViewDidEnter() {
    setTimeout(() => {
      const objdata = {
        id: this.utility.getLoggedUser('_id')
      };
      this._services.getNotification(objdata).subscribe((response: any) => {
        const res = response.response.data;
        this.notificationList = res.length;
      }, (error) => {
      });
    }, 8000);
    setTimeout(() => {
      if (localStorage.getItem('addshown') === null) {
        this.PromotionModal();
        localStorage.setItem('addshown', '1');
      }
    }, 20000);
  }
  goToPostPage(item) {
    // if (item.title === 'Doctors & Clinic') {
    //   this.navCtrl.push('DoctorbookingPage');
    // } else {
    this.navCtrl.push('PostPage', { id: item._id, title: item.title, image: item.image });
    // }
  }
  goToNotification() {
    this.navCtrl.push('NotificationPage');
  }
  openMenu() {
    this.events.publish('userdetails');
    this.menuCtrl.open();
  }
  goToSearch() {
    this.navCtrl.push('SearchPage');
  }
  saveDevicesToken() {
    const obj = {
      devicesid: localStorage.getItem('devices_token'),
      userid: this.utility.getLoggedUser('_id'),
      user_type: 'customer',
    };
    this._services.saveDevicesToken(obj).subscribe((response) => {
    });
  }
  showPromoterCategory(item) {
    if (item[0].bannerindex === 1) {
      this.navCtrl.parent.select(2);
      this.events.publish('grocery', '2');
    }
    if (item[0].bannerindex === 2) {
      this.navCtrl.parent.select(2);
    }
    if (item[0].bannerindex === 3) {
      this.navCtrl.push('PromoterCategoryPage');
    }
  }
  goToPost(item) {
    if (item.linkto === null) {
      return false;
    }
    this.navCtrl.push('PostdetailsPage', { id: item.linkto.id });
  }
  ionViewWillLeave() {
  }
  PromotionModal() {
    const modal = this.modalCtrl.create('PromotionModalPage');
    modal.present();
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  shopnow() {
    const item = {
      _id: '5b05aefe5c669d0676a6b1f6',
      title: 'Restaurant'
    };
    this.navCtrl.push('MyshopPage', { id: item._id, title: item.title });
  }
  goToPage(page: any) {
    this.navCtrl.push(page);
  }
  goToSearchBar() {
    this.navCtrl.setRoot('RestaurantSearchbarPage');
  }
  goToOfferzonePage() {
    this.navCtrl.setRoot('OfferzonePage');
  }
  goToMeatSearchPage() {
    this.navCtrl.setRoot('MeatSearchbarPage');
  }
  showComingSoon() {
    const toast = this.toastCtrl.create({
      message: 'We are Coming Soon!!!',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  goToGrocerySearchPage() {
    this.navCtrl.setRoot('GroceryEssentialPage');
  }
}
