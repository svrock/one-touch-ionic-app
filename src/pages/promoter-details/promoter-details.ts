import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage({
  segment: 'page-promoter-details/id/:id/type/:type'
})
@Component({
  selector: 'page-promoter-details',
  templateUrl: 'promoter-details.html',
})
export class PromoterDetailsPage {
  promoterList: any = [];
  constructor(public navCtrl: NavController, private socialSharing: SocialSharing,
    public _services: ServicesProvider, public toast: UtilityProvider,
    public navParams: NavParams) {
  }
  ionViewDidLoad() {
    const obj = {
      id: this.navParams.data.id
    };
    this._services.getPromoterDetails(obj).subscribe((response: any) => {
      const result = response.response.data;
      const promoterData = [];
      result.forEach(element => {
        const reviewcount = element.promoter.review;
        const val = {};
        val['promoter'] = element.promoter.promoter;
        val['image'] = element.image;
        val['comment'] = reviewcount.length;
        if (reviewcount.length < 1) {
          val['rating'] = 0;
        } else {
          let s = 0;
          // tslint:disable-next-line: no-shadowed-variable
          reviewcount.forEach(element => {
            const sum = element.rating;
            s += sum;
            const ratingcount = s / reviewcount.length;
            val['rating'] = ratingcount.toFixed(1);
          });
        }
        promoterData.push(val);
      });
      this.promoterList = promoterData;
    });
  }
  addReview(item) {
    this.navCtrl.push('ReviewPage', { id: item.promoter.id, type: 'promoter' });
  }
  openGalley(item) {
    this.navCtrl.push('GalleryPage', { id: item.id, type: 'promoter' });
  }
  share(data) {
    const obj = {
      message: 'Check this out ',
      subject: data.promoter.title,
      file: `http://139.59.12.86:1337/images/Promoter/${data.image[0].images}`,
      url: `http://139.59.12.86:1337/images/Promoter/${data.image[0].images}`
    };
    this.socialSharing.share(obj.message, obj.subject, obj.file, obj.url).then(() => {
    }).catch(() => {
    });
  }
}
