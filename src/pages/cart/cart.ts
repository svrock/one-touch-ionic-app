import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController, Events } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { GeoLocationProvider } from '../../providers/geo-location/geo-location';
import { UtilityProvider } from '../../providers/utility/utility';
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {
  subTotal: number;
  checkoutList: any[];
  address: any = [];
  paymenttype: string;
  shop: any = {};
  cartEmpty = true;
  offerDiscount: any;
  discountedValue: any;
  dicountedPrice: any;
  public isDiscountedOffer: boolean;
  constructor(public events: Events,
    public viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public utility: UtilityProvider,
    public _geoServices: GeoLocationProvider,
    public navCtrl: NavController,
    public navParams: NavParams, public _services: ServicesProvider) {
  }
  ionViewDidEnter() {
    const obj = {
      id: this.utility.getLoggedUser('_id')
    };
    this._services.getCart(obj).subscribe((response: any) => {
      let result = response.response.data;
      result = result.filter((item) => item.shopid);
      const pro = [];
      this.subTotal = 0;
      result.forEach(element => {
        const newpro = {};
        newpro['price'] = element.price;
        newpro['product_name'] = element.productid.title;
        newpro['product_id'] = element.productid.id;
        newpro['quantity'] = element.quantity;
        newpro['shopid'] = element.shopid.title;
        newpro['id'] = element.id;
        newpro['discount'] = element.productid.discount;
        this.shop.name = element.shopid.title;
        this.shop.id = element.shopid.id;
        if (element.productid.discount === 0) {
          newpro['isDiscountedOffer'] = false;
          this.subTotal += element.quantity * element.price;
        } else {
          newpro['isDiscountedOffer'] = true;
          this.offerDiscount = element.productid.discount;
          this.dicountedPrice = element.price - this.offerDiscount;
          newpro['discountedprice'] = this.dicountedPrice;
          this.subTotal += element.quantity * this.dicountedPrice;
        }
        pro.push(newpro);
      });
      pro.sort((a, b) => a.product_name.localeCompare(b.product_name));
      this.checkoutList = pro;
      this.cartEmpty = (result && result.length === 0) ? true : false;
    }, (_error) => {
      this.cartEmpty = true;
    });
    const data = {
      userid: this.utility.getLoggedUser('_id')
    };
    this._services.getShippingAddress(data).subscribe((response: any) => {
      this.address = response.response.data;
    });
  }
  increment(i) {
    this.checkoutList[i].quantity++;
    this.subtotalcal();
    this.addToCart(i);
  }
  decrement(i) {
    if (this.checkoutList[i].quantity === 1) {
      this.removeCartItem(i);
      this.checkoutList[i].quantity--;
      this.subtotalcal();
      this.checkoutList.splice(i, 1);
      return false;
    }
    this.checkoutList[i].quantity--;
    this.subtotalcal();
    this.addToCart(i);
  }
  Checkout() {
    this._geoServices.getGeolocationUser(this.shop.id);
    this.navCtrl.push('ShippingAreaPage');
  }
  subtotalcal() {
    this.subTotal = 0;
    this.checkoutList.forEach(element => {
      const newpro = {};
      newpro['price'] = element.price;
      newpro['quantity'] = element.quantity;
      newpro['discount'] = element.discount;
      if (element.discount === 0) {
        newpro['isDiscountedOffer'] = false;
        this.subTotal += element.quantity * element.price;
      } else {
        newpro['isDiscountedOffer'] = true;
        this.offerDiscount = element.discount;
        this.dicountedPrice = element.price - this.offerDiscount;
        this.subTotal += element.quantity * this.dicountedPrice;
      }
    });
  }
  addToCart(i) {
    if (this.checkoutList[i].quantity === 0) {
      return false;
    }
    const obj = {
      quantity: this.checkoutList[i].quantity,
      productid: this.checkoutList[i].product_id,
      price: this.checkoutList[i].price,
      userid: this.utility.getLoggedUser('_id'),
      shopid: this.shop.id,
      status: 0
    };
    this._services.addToCart(obj).subscribe((response) => {
    });
  }
  removeCartItem(i) {
    const obj = {
      id: this.checkoutList[i].id
    };
    this._services.removeItem(obj).subscribe((response) => {
      this.events.publish('cartupdated', 'ngOnInit prodcuct');

    });
  }
  shopnow() {
    this.navCtrl.popToRoot();
    this.navCtrl.parent.select(2);
  }
  backButton() {
    this.navCtrl.parent.select(0);
  }
}
