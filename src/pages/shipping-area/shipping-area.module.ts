import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShippingAreaPage } from './shipping-area';
import { SearchfilterPipe } from '../../pipes/filter/searchfilter';

@NgModule({
  declarations: [
    ShippingAreaPage, SearchfilterPipe
  ],
  imports: [
    IonicPageModule.forChild(ShippingAreaPage),
  ],
})
export class ShippingAreaPageModule {}
