import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
/**
 * Generated class for the ShippingAreaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shipping-area',
  templateUrl: 'shipping-area.html',
})
export class ShippingAreaPage {
  shippingArea: any = [];
  area: any;
  q: string;
  constructor(public utility: UtilityProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public _services: ServicesProvider) {
  }

  ionViewDidLoad() {

    this._services.getShippingArea().subscribe((response: any) => {
      this.shippingArea = response.response.data;

    }, (error) => {
    });
  }
  saveShippingArea() {
    if (this.area) {
      localStorage.setItem('shippingArea', JSON.stringify(this.area));
      this.navCtrl.push('CheckoutPage');
    } else {
      const msg = {
        title: 'Shipping order',
        subtitle: 'Please select your shipping area.'
      };
      this.utility.showAlert(msg);
    }

  }

}
