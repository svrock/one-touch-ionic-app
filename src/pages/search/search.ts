import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {
  defaultImage = 'assets/imgs/searchplaceholder.png';
  offset = 1000;
  q = 'restaurants';
  spinner = true;
  textmessage = false;
  warmmessage = 'Search ...';
  postList: any = [];
  constructor(public utility: UtilityProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public _services: ServicesProvider) {
  }
  ionViewDidLoad() {
    const obj = {
      type: 'tags',
      q: this.q,
      region: localStorage.getItem('region')
    };
    this.searchList(obj);

  }
  search() {

    if (this.q.length === 0) {
      this.ionViewDidLoad();
    }
    if (this.q.length < 3) {
      this.textmessage = true;
      this.postList = [];
      return false;
    }
    this.textmessage = false;
    this.spinner = false;
    this.warmmessage = '';
    const obj = {
      type: 'tags',
      q: this.q
    };
    this.searchList(obj);

  }
  PostdetailsPage(item) {
    this.navCtrl.push('PostdetailsPage', { id: item });
  }

  searchList(obj) {
    this.warmmessage = 'Search ...';

    this._services.search(obj).subscribe((response: any) => {
      this.spinner = true;
      const result = response.data;
      const list = [];
      result.forEach(data => {
        this.warmmessage = '';

        const element = {};
        const resultdata = data.result;
        const prodictaAailable = data.category;
        if (resultdata.images) {
          element['images'] = resultdata.images;
        } else {
          element['images'] = 'assets/imgs/searchplaceholder.png';
        }
        element['prodict_available'] = prodictaAailable;
        element['title'] = resultdata.title;
        element['address'] = resultdata.address;
        element['_id'] = resultdata._id;
        element['rating'] = resultdata.rating;
        list.push(element);
      });
      list.sort((a, b) => a.title.localeCompare(b.title));
      this.postList = list;
      if (this.postList.length < 1) {
        this.textmessage = true;
        this.warmmessage = 'No search result found on "<b>' + this.q + '</b>" you can try buy ,sell etc.';
      }
    }, (error) => {
    });
  }
}
