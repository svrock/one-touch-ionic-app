import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
@Component({
  selector: 'page-cartcounter',
  templateUrl: 'cartcounter.html',
})
export class CartcounterPage {
  qty: number;
  constructor(public utility: UtilityProvider,
    public _services: ServicesProvider, public events: Events, public navCtrl: NavController, public navParams: NavParams) {
    events.subscribe('cartupdated', (data) => {
      this.countCartItem();
    });
  }
  ionViewDidLoad() {
    this.countCartItem();
  }
  goToCart() {
    if (this.qty < 1) {
      const msg = {
        msg: `Please add some item to your product`,
        duration: 3000
      };
      this.utility.showToast(msg);
      return false;
    }
    this.navCtrl.parent.select(3);
  }
  countCartItem() {
    const obj = {
      id: this.utility.getLoggedUser('_id')
    };
    this._services.getCart(obj).subscribe((response: any) => {
      this.qty = 0;
      let result = response.response.data;
      result = result.filter((item) => item.shopid);
      const pro = [];
      result.forEach(element => {
        this.qty += element.quantity;
      });
    }, (error) => {
      this.qty = 0;
    });
  }
  ngOnDestroy() {
    this.events.unsubscribe('cartupdated');
  }
}
