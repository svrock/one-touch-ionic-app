import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
import { config } from '../../config';
/**
* Generated class for the YourOrderPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
@IonicPage({
  segment: 'page-your-order/id/:id'
})
@Component({
  selector: 'page-your-order',
  templateUrl: 'your-order.html',
})
export class YourOrderPage {
  orderid: any;
  productList: any[];
  order: any = [];
  subTotal = 0;
  orderChatUrl: string;
  constructor(public utility: UtilityProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public _services: ServicesProvider) {
    this.orderid = this.navParams.data.id;
  }
  ionViewDidLoad() {
    this._services.getOrderDetails(this.orderid).subscribe((response: any) => {
      const result = response.response.data;
      const cart = result[0].cart;
      this.order = result[0].order;
      const cartItem = [];
      cart.forEach(element => {
        const item = {};
        item['product_name'] = element.productid.title;
        item['product_price'] = element.productid.price;
        item['product_qty'] = element.quantity;
        this.subTotal += element.quantity * element.price;
        cartItem.push(item);
      });
      this.productList = cartItem;
      this.getWhatsAppMessage();
    }, (error) => {
    });
  }
  getWhatsAppMessage() {
    const productlist = this.productList.map(item => `${item.product_qty}   ${item.product_name}`);
    // tslint:disable-next-line: max-line-length
    const url = `https://wa.me/${config.agentNumber}?text=${config.ORDER_CUSTOM_MESSAGE} ${this.order.orderid} --- ${productlist.toString()} from ${this.order.shopid.title}`;
    this.orderChatUrl = url;
  }
}
