import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
/**
 * Generated class for the ProfilePage page.
 *
 * See https:
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  user: any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams, private geolocation: Geolocation) {
  }
  ngOnInit(): void {
  }
  ngAfterViewInit() {
    this.loadMap();
  }
  ionViewDidLoad() {
    const result = this.navParams.data.data;
    this.user = {
      name: result.firstname + result.lastname,
      profileImage: result.profile_image,
      coverImage: '',
      occupation: result.shopname,
      location: result.city + result.state + result.country,
      description: '',
      address: result.address + ' ' + result.city + ' ' + result.state + ' ' + result.country,
      phone: result.phone,
      email: result.email,
      whatsapp: result.phone,
      shopname: result.shopname,
      userid: result._id
    };
  }
  loadMap() {
  }
  goToReview() {
    this.navCtrl.push('ReviewPage', { id: this.user.userid });
  }
  openChat(item) {
    this.navCtrl.push('ChatPage', item);
  }
}
