import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import { Geolocation } from '@ionic-native/geolocation';


@NgModule({
  declarations: [
    ProfilePage,
  ],
  providers: [Geolocation],
  imports: [
    IonicPageModule.forChild(ProfilePage),
  ],
})
export class ProfilePageModule {}
