import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController, ViewController, AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { UtilityProvider } from '../../providers/utility/utility';
/**
* Generated class for the QtyModalPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
@IonicPage()
@Component({
  selector: 'page-qty-modal',
  templateUrl: 'qty-modal.html',
})
export class QtyModalPage {
  constructor(
    public navParams: NavParams, public utility: UtilityProvider, public navCtrl: NavController,
    public _services: ServicesProvider,
    public events: Events,
    public viewCtrl: ViewController, private alertCtrl: AlertController,
    public modalCtrl: ModalController) {
    this.productid = this.navParams.data.id;
  }
  productid: any;
  shopList: any[];
  private qty = 1;
  ionViewDidLoad() {
    const obj = {
      id: this.utility.getLoggedUser('_id')
    };
    this._services.getCart(obj).subscribe((response: any) => {
      let result = response.response.data;
      result = result.filter((item) => item.shopid);
      const shoplist = [];
      result.forEach(element => {
        if (shoplist.indexOf(element.shopid.id) === -1) {
          shoplist.push(element.shopid.id);
        }
      });
      this.shopList = shoplist;
    }, (error) => {
      this.shopList = [];
    });
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  increment(i) {
    this.qty++;
  }
  decrement(i) {
    if (this.qty === 0) {
      return false;
    }
    this.qty--;
  }
  addToCart() {
    if (this.qty === 0) {
      return false;
    }
    if (this.shopList.indexOf(this.productid.product_category.shopid) === -1 && this.shopList.length > 0) {
      const alert = this.alertCtrl.create({
        title: 'Replace cart item ? ',
        message: 'There are item in your cart .Do you want to replace with them?',
        buttons: [
          {
            text: 'cancel',
            role: 'cancel',
            handler: () => {
            }
          },
          {
            text: 'Replace',
            handler: () => {
              const obj = {
                shopid: this.shopList,
                userid: this.utility.getLoggedUser('_id')
              };
              this._services.rePlaceCart(obj).subscribe((response) => {
                this.shopList = [];
                this.addToCart();
              });
            }
          }
        ]
      });
      alert.present();
    } else {
      const obj = {
        quantity: this.qty,
        productid: this.productid.id,
        price: this.productid.price,
        userid: this.utility.getLoggedUser('_id'),
        shopid: this.productid.product_category.shopid,
        status: 0
      };
      this._services.addToCart(obj).subscribe((response) => {
        const msg = {
          msg: ` ${this.productid.title} added ${this.qty}`,
          duration: 3000
        };
        this.utility.showToast(msg);
        this.events.publish('addToCart', 'addToCart quantity');
      });
    }
    this.viewCtrl.dismiss();
  }
}
