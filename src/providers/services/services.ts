import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { catchError, publishReplay, refCount } from 'rxjs/operators';
import { config } from '../../config';
import { Response } from '@angular/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
/*
Generated class for the ServicesProvider provider.
See https://angular.io/guide/dependency-injection for more info on providers
and Angular DI.
*/
@Injectable()
export class ServicesProvider {
  userdata: any;
  shopItem: any;
  getPromoterLocation: any;
  userDeatilsData: any;
  bannerData: any;
  category: any;
  getShopProductData: any;
  region: any;
  tagList: any;
  offerItem: any;
  constructor(private http: HttpClient, ) { }
  private handleError(error: HttpErrorResponse) {

    // return an ErrorObservable with a user-facing error message+
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  }
  getCategory() {
    if (!this.category) {
      this.category = this.http.get(config.API_ENDPOINT + '/get-category')
        .pipe(catchError(this.handleError), publishReplay(1),
          refCount());
    }
    return this.category;
  }
  getVendor() {
    return this.http.get(config.API_ENDPOINT + '/get-vendor');
  }
  getFacebookGraphApi(data) {
    // tslint:disable-next-line: max-line-length
    return this.http.get('https://graph.facebook.com/me/?fields=picture.type(large),email,about,first_name,last_name,address,birthday,cover,age_range&access_token=' + data.authResponse.accessToken);
  }
  //
  // ──────────────────────────────────────────────────────────────────── XIII ──────────
  //   :::::: F A C E B O O K   L O G I N : :  :   :    :     :        :          :
  // ──────────────────────────────────────────────────────────────────────────────
  //
  login(data) {
    return this.http.post(config.API_ENDPOINT + '/fblogin', data);
  }
  doLoginWithPhoneNumber(data) {
    return this.http.post(config.API_ENDPOINT + '/login-with-phone', data);
  }
  getaddress(data) {
    return this.http.get(config.MAP + '&latlng=' + data.latitude + ',' + data.longitude);
  }
  addreview(data) {
    return this.http.post(config.API_ENDPOINT + '/add-review', data);
  }
  getreview(data) {
    return this.http.get(`${config.API_ENDPOINT}/get-review?id=${data.id}&type=${data.type}`);
  }
  getChatHistory(obj) {
    return this.http.get(config.API_ENDPOINT + '/chatHistory?remoteUserId=' + obj.remoteuserid + '&page=' + obj.page);
  }
  //
  // ─── GET CHAT USER LIST ─────────────────────────────────────────────────────────
  //
  getChatUserLIst() {
    return this.http.get(config.API_ENDPOINT + '/chat-user-list');
  }
  getpromoter() {
    return this.http.get(config.API_ENDPOINT + '/get-promoter');
  }
  search(data) {
    return this.http.post(config.API_ENDPOINT + '/search', data);
  }
  getPostDetails(data) {
    return this.http.get(config.API_ENDPOINT + '/get-post-details?id=' + data);
  }
  getPost() {
    return this.http.get(`${config.API_ENDPOINT}/get-post`);
  }
  doLogin(data) {
    return this.http.post(config.API_ENDPOINT + '/login', data);
  }
  register(data) {
    return this.http.post(config.API_ENDPOINT + '/register', data);
  }
  updateuser(data) {
    return this.http.post(config.API_ENDPOINT + '/updateuser', data);
  }
  getUserDetails() {
    return this.userDeatilsData = this.http.get(config.API_ENDPOINT + '/userdetails')
      .pipe(catchError(this.handleError), publishReplay(1),
        refCount());
  }
  forgetPassword(data) {
    return this.http.post(`${config.API_ENDPOINT}/forget-password`, data);
  }
  changePassword(data) {
    return this.http.post(`${config.API_ENDPOINT}/change-password`, data);
  }
  feedback(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/feedback`, data);
  }
  getCms(data) {
    return this.http.get(`${config.API_ENDPOINT}/get-cms?pagename=${data}`);
  }
  getTagsById(data) {
    return this.http.get(`${config.API_ENDPOINT}/get-tags-by-id?id=${data.id}`);
  }
  getProductCategory(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-product-category?shopid=${data.shopid}`);
  }
  getShopProduct() {
    if (!this.shopItem) {
      const region = localStorage.getItem('region');
      this.shopItem = this.http.get(`${config.SAIL_ENDPOINT}/shop-product?region=${region}`)
        .pipe(catchError(this.handleError), publishReplay(1),
          refCount());
    }
    return this.shopItem;
  }
  getProduct(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-product?id=${data}`);
  }
  addToCart(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/add-to-cart`, data);
  }
  getCart(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-cart`);
  }
  getCartItem(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-cart-item`);
  }
  shippingAddress(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/add-shipping-address`, data);
  }
  getShippingAddress(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-shipping-address?id=${data.userid}`);
  }
  makePaymenrtRequest(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/payment-link`, data);
  }
  createOrder(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/create-order`, data);
  }
  getOrderDetails(id) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-order?id=${id}`);
  }
  getOrderList(id) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-order-list?id=${id}`);
  }
  removeItem(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/remove-cart-item`, data);
  }
  saveDevicesToken(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/add-devices`, data);
  }
  getOrder(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-order?id=${data.id}`);
  }
  getNotification(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-notification?id=${data.id}`);
  }
  MarkSeenNotification(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/mark-read-notification`, data);
  }
  getPromoterCategory() {
    if (!this.getPromoterLocation) {
      this.getPromoterLocation = this.http.get(`${config.SAIL_ENDPOINT}/get-promoter-location`)
        .pipe(catchError(this.handleError), publishReplay(1),
          refCount());
    }
    return this.getPromoterLocation;
  }
  getPromoterDetails(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-promoter?id=${data.id}`);
  }
  getPromoterImage(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-promoter-image?id=${data.id}`);
  }
  getBanner() {
    if (!this.bannerData) {
      this.bannerData = this.http.get(`${config.SAIL_ENDPOINT}/get-banner`)
        .pipe(catchError(this.handleError), publishReplay(1),
          refCount());
    }
    return this.bannerData;
  }
  getUserRating(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-user-review?id=${data.id}&type=${data.type}&userid=${data.userid}`);
  }
  updatereviewdata(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/update-review`, data);
  }
  sendOTP(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/shipping-phone-otp`, data);
  }
  verifyOTP(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/check-phone-otp`, data);
  }
  rePlaceCart(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/replace-cart`, data);
  }
  sendRequestVendorJoin(data) {
    return this.http.post(`${config.SAIL_ENDPOINT}/vendor-join-request`, data);
  }
  getLocation() {
    if (!this.region) {
      this.region = this.http.get(`${config.API_ENDPOINT}/get-region`)
        .pipe(catchError(this.handleError), publishReplay(1),
          refCount());
    }
    return this.region;
  }
  getTag() {
    if (!this.tagList) {
      this.tagList = this.http.get(`${config.SAIL_ENDPOINT}/tag`)
        .pipe(catchError(this.handleError), publishReplay(1),
          refCount());
    }
    return this.tagList;
  }
  getDeliveryCharges(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-shipping-charges?userid=${data.userid}&shopid=${data.shopid}`);
  }
  getShippingArea() {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-setting`);
  }
  getOfferItemsList() {
    if (!this.offerItem) {
      const region = localStorage.getItem('region');
      this.offerItem = this.http.get(`${config.SAIL_ENDPOINT}/offer-product?region=${region}`)
        .pipe(catchError(this.handleError), publishReplay(1),
          refCount());
    }
    return this.offerItem;
  }
  getProductbyResturant(data) {
    return this.http.get(`${config.SAIL_ENDPOINT}/get-resturant-product?shopid=${data}`);
  }
  getPostForMeatCat(data) {
    return this.http.get(`${config.API_ENDPOINT}/get-post?catid=${data.id}`);
  }
}
