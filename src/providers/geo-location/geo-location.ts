import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { config } from '../../config';
import { ServicesProvider } from '../services/services';
import { UtilityProvider } from '../utility/utility';
import { AlertController } from 'ionic-angular';
/*
  Generated class for the GeoLocationProvider provider.
  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GeoLocationProvider {
  constructor(public http: HttpClient,
    public _services: ServicesProvider, public alertCtrl: AlertController,
    private geolocation: Geolocation, public utility: UtilityProvider,
  ) {
  }
  permissionAlert(shopid) {
    const confirm = this.alertCtrl.create({
      title: config.geolocationTitle,
      message: config.geolocationDescription,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.getGeolocationUser(shopid);
          }
        }
      ]
    });
    confirm.present();
  }
  getGeolocationUser(shopid) {
    this.geolocation.getCurrentPosition().then((resp) => {
      const obj = {
        latitude: resp.coords.latitude,
        longitude: resp.coords.longitude
      };
      localStorage.setItem('location', JSON.stringify(obj));
      this.updateUserDetails(shopid);
    }).catch((err) => {
    });
  }
  updateUserDetails(shopid) {
    const userdata = JSON.parse(localStorage.getItem('userdetails'));
    userdata.location = JSON.parse(localStorage.getItem('location'));
    this._services.updateuser(userdata).subscribe((response) => {
    });
  }
}
