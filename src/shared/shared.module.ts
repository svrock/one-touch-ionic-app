import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicPageModule } from 'ionic-angular';
import { GhostElementPage } from '../pages/ghost-element/ghost-element';
import { CartcounterPage } from '../pages/cartcounter/cartcounter';

@NgModule({
  imports: [
    CommonModule,
    IonicPageModule,
  ],
  declarations: [CartcounterPage,
    GhostElementPage,
  ],
  exports: [CartcounterPage,
    GhostElementPage,
  ]
})
export class SharedModule { }
