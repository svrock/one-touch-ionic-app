import { Injectable, ViewChild } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { Events } from 'ionic-angular';
import { UtilityProvider } from '../providers/utility/utility';
import { Network } from '@ionic-native/network';
@Injectable()
export class Interceptor implements HttpInterceptor {
  constructor(public events: Events, public utility: UtilityProvider, private network: Network) { }
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const hostname = (new URL(req.url)).hostname;
    let cloneReq;
    if (this.network.type === 'none') {
      const msg = { msg: 'Please check your internet connection', duration: 3000, position: 'centre' };
      this.utility.showToast(msg);
    }
    if (hostname !== 'maps.googleapis.com') {
      const headers = req.headers
        .set('Content-Type', 'application/json')
        .set('x-access-token', `${localStorage.getItem('token')}`)
        .set('region', `${localStorage.getItem('region')}`);
      cloneReq = req.clone({ headers });
    } else {
      cloneReq = req.clone({});
    }
    return next.handle(cloneReq).do((event: HttpEvent<any>) => {
      // if (event instanceof HttpResponse) {
      // }
    }, (err: any) => {
      if (err instanceof HttpErrorResponse) {
        if (err.error.message === 'failed to authenticate') {
          this.events.publish('user:created', 'user', Date.now());
        }
      }
    });
  }
}
