import { Component, ViewChild, enableProdMode } from '@angular/core';
import { Platform, Nav, Events, AlertController, App } from 'ionic-angular';
import { ServicesProvider } from '../providers/services/services';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AppVersion } from '@ionic-native/app-version';
enableProdMode();
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  user: any = {};
  rootPage: string;
  isDevices: boolean;
  constructor(platform: Platform, private appVersion: AppVersion,
    public events: Events, public alertCtrl: AlertController,
    private iab: InAppBrowser,
    public _services: ServicesProvider, public app: App) {
    events.subscribe('user:created', (user, time) => {
      this.logout();
    });
    /**Code for handling hardware back button in ionic3**/
    platform.ready().then((res) => {
      platform.registerBackButtonAction(() => {
        const nav = app.getActiveNavs()[0];
        if (nav && nav.canGoBack()) {
          nav.pop();
        } else {
          const alert = this.alertCtrl.create({
            title: 'Exit Application?',
            message: 'Do you want to exit the application?',
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
                handler: () => {
                }
              },
              {
                text: 'Exit',
                handler: () => {
                  platform.exitApp();
                }
              }
            ]
          });
          alert.present();
        }
    });
      this.isDevices = platform.is('cordova');
      const logincheck = localStorage.getItem('token');
      // this._services.getGeolocation();
      const checkApptour = localStorage.getItem('apptour');
      if (checkApptour === 'seen' && logincheck !== null) {
        this.rootPage = 'WelcomePage';
        this.getUserDetails();
      } else {
        this.rootPage = 'ApptourPage';
      }
    });
    events.subscribe('userdetails', () => {
      this.getUserDetails();
    });
  }
  getUserDetails() {
    this._services.getUserDetails().subscribe((response) => {
      localStorage.setItem('userdetails', JSON.stringify(response.data));
      this.user = response.data;
      if (this.isDevices) {
        this.appVersion.getVersionNumber().then((getVersionNumber) => {
          if (response.appVersion > getVersionNumber) {
            this.showUpdateConfirm();
          }
        });
      }
      if (!this.user.email) {
        this.logout();
      }
    });
  }
  goToPage(page: string) {
    if (page === 'WelcomePage') {
      this.nav.setRoot(page);
    } else {
      this.nav.push(page);
    }
  }
  CmsPage(page) {
    this.nav.push('CmsPage', { pagename: page });
  }
  logout() {
    localStorage.clear();
    this.nav.setRoot('LoginPage');
  }
  goToChat() {
    this.nav.push('ChatPage', { id: '5b1c0d796728f530bbd321dd' });
  }
  showUpdateConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'Update Available',
      message: 'Update from play store',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
          }
        },
        {
          text: 'Update',
          handler: () => {
            window.open('market://details?id=com.baharampur.onetouch', '_system');
          }
        }
      ]
    });
    confirm.present();
  }
}
