import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { ServicesProvider } from '../providers/services/services';
import { Facebook } from '@ionic-native/facebook';
import { Geolocation } from '@ionic-native/geolocation';
import { UtilityProvider } from '../providers/utility/utility';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Interceptor } from './interceptor';
import { Toast } from '@ionic-native/toast';
import { Camera } from '@ionic-native/camera';
import * as ionicGalleryModal from 'ionic-gallery-modal';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Network } from '@ionic-native/network';
import { MyshopPage } from '../pages/myshop/myshop';
import { RestaurantPage } from '../pages/restaurant/restaurant';
import { HomePage } from '../pages/home/home';
import { ProductPage } from '../pages/product/product';
import { FilterPipe } from '../pipes/filter/filter';
import { DefaultImagePipe } from '../pipes/default-image/default-image';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CartPage } from '../pages/cart/cart';
import { GeoLocationProvider } from '../providers/geo-location/geo-location';
import { AppVersion } from '@ionic-native/app-version';
import { DoctorbookingPage } from '../pages/doctorbooking/doctorbooking';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [MyshopPage, RestaurantPage, CartPage,
    MyApp, HomePage, ProductPage, FilterPipe,
    DoctorbookingPage,
    DefaultImagePipe
  ],
  imports: [ionicGalleryModal.GalleryModalModule, SharedModule,
    BrowserModule, HttpClientModule,
  IonicModule.forRoot(MyApp, {
    backButtonIcon: 'ios-arrow-back',
    backButtonText: '',
    pageTransition: 'md-transition',
    activator: 'ripple',
    mode: 'md',
    tabsHideOnSubPages: true
  })
  ],
  bootstrap: [IonicApp],
  entryComponents: [RestaurantPage, CartPage, DoctorbookingPage,
    MyApp, MyshopPage, HomePage, ProductPage,
  ],
  // tslint:disable-next-line: deprecation
  providers: [FileTransfer, File, Camera, AppVersion,
    InAppBrowser,
    Facebook, Geolocation, Toast,
    { provide: ErrorHandler, useClass: IonicErrorHandler, },
    ServicesProvider,
    Network,
    UtilityProvider,
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: ionicGalleryModal.GalleryModalHammerConfig,
    },
    GeoLocationProvider,
  ]
})
export class AppModule { }
